/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuckoo;

/**
 *
 * @author acer
 */
public class TestCuckoo {

    public static void main(String[] args) {
        
        Cuckoo cuckoo = new Cuckoo();
        cuckoo.add(5, "JM");
        cuckoo.add(4, "Mint");
        cuckoo.add(2, "JENT");
        cuckoo.put(19,"AP");
        cuckoo.put(33, "PSS");
        cuckoo.put(121,"TSF");
        
        System.out.println(cuckoo.get(5).toString());
        System.out.println(cuckoo.get(4).toString());
        System.out.println(cuckoo.get(2).toString());
        System.out.println(cuckoo.get(19).toString());
        System.out.println(cuckoo.get(33).toString());
        System.out.println(cuckoo.get(121).toString());
        
        System.out.println("------Delete------");
        cuckoo.delete(33);
        System.out.println(cuckoo.get(33));

}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuckoo;

/**
 *
 * @author acer
 */
public class Cuckoo {

    //Declare the scope of the variable.
    private int key;
    private String value;

    private int n = 100;  //Set the size of each table equal to n = 100
    Cuckoo[] T1 = new Cuckoo[n];
    Cuckoo[] T2 = new Cuckoo[n];

    //Executing parameters within a class
    public Cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckoo() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hash1(int key) {
        return key % T1.length;
    }

    public int hash2(int key) {
        return (key / n) % T2.length;
    }

    public void put(int key, String value) { //method for put values ​​to each table

        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            T1[hash1(key)].key = key;
            T1[hash1(key)].value = value;
            return;
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            T2[hash2(key)].key = key;
            T2[hash2(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T1[hash1(key)] == null) {
                    T1[hash1(key)] = new Cuckoo();
                    T1[hash1(key)].key = key;
                    T1[hash1(key)].value = value;
                    return;
                }
            } else {
                if (T2[hash2(key)] == null) {
                    T2[hash2(key)] = new Cuckoo();
                    T2[hash2(key)].key = key;
                    T2[hash2(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                String tempValue = T1[hash1(key)].value;
                int tempKey = T1[hash1(key)].key;
                T1[hash1(key)].key = key;
                T1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else { 
                String tempValue = T2[hash2(key)].value;
                int tempKey = T2[hash2(key)].key;
                T2[hash2(key)].key = key;
                T2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public void add(int key, String value) {  //method for adding values ​​to each table
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            T1[hash1(key)].key = key;
            T1[hash1(key)].value = value;
            return;
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            T2[hash2(key)].key = key;
            T2[hash2(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T1[hash1(key)] == null) {
                    T1[hash1(key)] = new Cuckoo();
                    T1[hash1(key)].key = key;
                    T1[hash1(key)].value = value;
                    return;
                }
            } else {
                if (T2[hash2(key)] == null) {
                    T2[hash2(key)] = new Cuckoo();
                    T2[hash2(key)].key = key;
                    T2[hash2(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                String tempValue = T1[hash1(key)].value;
                int tempKey = T1[hash1(key)].key;
                T1[hash1(key)].key = key;
                T1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else {
                String tempValue = T2[hash2(key)].value;
                int tempKey = T2[hash2(key)].key;
                T2[hash2(key)].key = key;
                T2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public Cuckoo get(int key) {
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            return T1[hash1(key)];
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            return T2[hash2(key)];
        }
        return null;
    }

    public Cuckoo delete(int key) {  //method for delete values ​​to each table
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            Cuckoo tmp = T1[hash1(key)];
            T1[hash1(key)] = null;
            return tmp;
        }
        if (T1[hash2(key)] != null && T1[hash2(key)].key == key) {
            Cuckoo tmp = T1[hash2(key)];
            T1[hash2(key)] = null;
            return tmp;
        }
        return null;
    }

    @Override
    public String toString() {   //return message display
        return "Key -> " + key + " Value -> " + value;
    }

}
